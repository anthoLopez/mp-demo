const express = require("express");
const cors = require("cors");
const morgan = require("morgan");

// SDK de Mercado Pago
const mercadopago = require("mercadopago");

const app = express();
const port = 3000;

app.use(morgan("dev"));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use(express.static("./public"));

app.set("views", "./views");
app.set("view engine", "pug");

// Agrega credenciales
mercadopago.configure({
  access_token:
    "TEST-1010593094666935-083007-68102c944d3f6d5f955c72605f872b62-815167266",
});

app.get("/", (req, res) => {
  res.render("index", {
    title: "Mi City Slin Bag / Xiaomi Mochila Morral | mp - demo",
    message: "Hello there!",
  });
});

app.post("/checkout", (req, res) => {
  // Crea un objeto de preferencia
  console.log("Request: ", req.body);

  let preference = {
    items: [
      {
        title: req.body.title,
        picture_url: "http://localhost:3000/images/img1.jpg",
        unit_price: parseInt(req.body.price),
        quantity: 1,
      },
    ],
    back_urls: {
      success: "http://localhost:3000/feedback",
      failure: "http://localhost:3000/",
      pending: "http://localhost:3000/feedback",
    },
    auto_return: "approved",
    Notification_url:
      "https://webhook.site/834d6ffa-2b9b-4a97-99b2-ab0afc6d8ae6",
  };

  mercadopago.preferences
    .create(preference)
    .then(function (response) {
      console.log("view response mp: ", response.body);
      res.redirect(response.body.init_point);
    })
    .catch(function (error) {
      console.log(error);
    });
});

app.get("/feedback", (req, res) => {
  console.log("View process payment: ", req.query);
  res.render("res-payment", {
    payment: req.query.payment_id,
    status: req.query.status,
    merchantOrder: req.query.merchant_order_id,
  });
});

app.post("/ipn", (req, res) => {
  console.log("View response notification: ", req.query);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
